# Руководство

**Назначение**

Шаблон для верстки html сайтов с подключенными библиотеками: bootstrap4, fancybox, jquery, jquery-ui, angularjs, slick, font-awesome, glyphicons.

**Зависимости**

- Git
- NodeJS
- NPM
- Gulp

**Установка Git**

[Download and Install Git](https://git-scm.com/download/)

**Установка NodeJS и NPM**

[Download and Install NodeJS](https://nodejs.org/en/download/)

**Установка Gulp**

```
npm i gulp -g
```

**Клонирование репозитория**

```
git clone git@bitbucket.org:ntropin/affetta-blank.git projectname
```

**Перейти в каталог проекта**

```
cd projectname
```

**Установка сторонних библиотек**

```
npm install
```

Команда `npm install` установит необходимые сторонние библиотеки, указанные в **package.json** в директорию **node_modules**.

**Сборка проекта**

```
gulp
```

Команда `gulp` соберет файлы проекта в директорию **web/build** и запустит сервер по адресу http://localhost:3000. 

## Структура файлов

- resources
    - assets
        - fonts
        - img
        - js
            - app
                - *app.js*
            - vendor
                - *vendor.js*
        - style
            - app
                - *app.scss*
            - vendor
                - *vendor.scss* 
                - *variables.scss*
    - html   
        - includes
        - *index.html*
- node_modules
- web
    - build
        - css
        - fonts
        - img
        - js
    - *index.html*
- *gulpfile.js*
- *package.json*   

### resource/assets/  

Директория содержит исходники пользовательских скрипов (js), стилей (style), шрифтов (fonts) и картонок (img).

**resource/assets/fonts/** - Директория с пользовательскими шрифтами.

**resource/assets/img/** - Директория для исходных картинок, использующихся в верстке.

### resource/assets/js/

Директория содержит пользовательские и стороние скрипты, подключаемые к проекту.

**resource/assets/js/app/** - Директория для пользовательских скриптов. 

**resource/assets/js/vendor/** - Директория для сторонних скриптов.

**resource/assets/js/app/app.js** - В файле подключаются все пользовательские скрипты.

**resource/assets/js/vendor/vendor.js** - В файле подключаются все сторонние скрипты.

### resource/assets/style/

Директория содержит стили, подключаемые к проекту.

**resource/assets/style/app/** - Директория для пользовательских стилей.

**resource/assets/style/vendor/** - Директория для сторонних стилей.

**resource/assets/style/app.scss** - В файле подключаются все пользовательские стили.

**resource/assets/style/vendor/vendor.scss** - В файле подключаются все сторонние стили.

### resource/html/

Директория содержит исходные html файлы проекта.

### node_modules/

Директория содержит сторонние Javascript библиотеки (Jquery, Bootstrap, Fancybox и пр.), управление которыми осуществляется через менеджер пакетов **npm**.

**Пример установки сторонних javascript библиотек**

```
npm install jquery --save
```

После установки пакета надо подключить скрипты и стили библиотеки к проекту.

Скрипты прописываются в файле - *resource/assets/js/vendor/vendor.js*

Стили прописываются в файле - *resource/assets/style/vendor/vendor.scss*

Если библиотека требует подключение своих шрифтов и картинок, то нужно добавить задачи в *gulpfile.js*

### web/build/

Директория создается командой `gulp` и содержит минифицированные скрипты, стили, картинки и шрифты.
 
### gulpfile.js

Конфигурационный файл для сборщика **Gulp**. Собирает и минифицирует скрипты и стили. Запускает сервер.

### package.json

Конфигурационный файл для менеджера пакетов `npm`. Описывает внешние зависимости проекта.  