(function () {
    'use strict';

    angular
        .module('app')
        .config(config);

    config.$inject = ['$locationProvider'];
    /* @ngInject */
    function config($locationProvider) {
        $locationProvider.html5Mode(false);
    }
})();