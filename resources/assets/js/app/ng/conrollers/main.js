(function () {
    'use strict';

    angular
        .module('app')
        .controller('Main', Main);

    Main.$inject = [];

    /* @ngInject */
    function Main() {
        /* jshint validthis: true */
        var vm = this;

        vm.name = 'Bootstrap 4 and AngularJS';

        activate();

        function activate() {

        }
    }
})();