$(document).ready(function () {

    $(".datepicker").datepicker({
        dateFormat: 'dd.mm.yy',
        inline: true,
        language: 'ru',
        changeYear: true,
        changeMonth: true
    });
});