// Import AngularJS
//=require ../../../../node_modules/angular/angular.js
//=require ../../../../node_modules/angular-resource/angular-resource.js
//=require ../../../../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js
//=require ../../../../node_modules/angular-mocks/angular-mocks.js
//=require ../../../../node_modules/angular-sanitize/angular-sanitize.js
//=require ../../../../node_modules/angular-bootstrap-confirm/dist/angular-bootstrap-confirm.js
//=require ../../../../node_modules/angucomplete-alt/angucomplete-alt.js

// Import jQuery
//=require ../../../../node_modules/jquery/dist/jquery.js

// Import Popper
//=require ../../../../node_modules/popper.js/dist/umd/popper.js

// Import jQuery-UI
//=require ../../../../node_modules/jquery-ui/ui/version.js
//=require ../../../../node_modules/jquery-ui/ui/widget.js
//=require ../../../../node_modules/jquery-ui/ui/position.js
//=require ../../../../node_modules/jquery-ui/ui/keycode.js
//=require ../../../../node_modules/jquery-ui/ui/unique-id.js
//=require ../../../../node_modules/jquery-ui/ui/widgets/autocomplete.js
//=require ../../../../node_modules/jquery-ui/ui/widgets/datepicker.js
//=require ../../../../node_modules/jquery-ui/ui/widgets/menu.js
//=require ../../../../node_modules/jquery-ui/ui/i18n/datepicker-ru.js

// Import Bootstrap 4
//=require ../../../../node_modules/bootstrap/js/dist/util.js
//=require ../../../../node_modules/bootstrap/js/dist/alert.js
//=require ../../../../node_modules/bootstrap/js/dist/button.js
//=require ../../../../node_modules/bootstrap/js/dist/carousel.js
//=require ../../../../node_modules/bootstrap/js/dist/collapse.js
//=require ../../../../node_modules/bootstrap/js/dist/dropdown.js
//=require ../../../../node_modules/bootstrap/js/dist/modal.js
//=require ../../../../node_modules/bootstrap/js/dist/scrollspy.js
//=require ../../../../node_modules/bootstrap/js/dist/tab.js
//=require ../../../../node_modules/bootstrap/js/dist/tooltip.js
//=require ../../../../node_modules/bootstrap/js/dist/popover.js

// Import Fancybox
//=require ../../../../node_modules/fancybox/dist/js/jquery.fancybox.js

// Import Slick
//=require ../../../../node_modules/slick-carousel/slick/slick.js