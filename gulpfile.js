"use strict";

/* Connect gulp and plugins */
var gulp = require('gulp'),  // Connect Gulp
    webserver = require('browser-sync'), // Server for work and auto update pages
    plumber = require('gulp-plumber'), // Plugin for tracking errors
    include = require('gulp-include'), // Plugin for import file to over file
    fileinclude = require('gulp-file-include'), // Plugin for file include
    sourcemaps = require('gulp-sourcemaps'), // Plugin for generate source maps
    sass = require('gulp-sass'), // Plugin for compile SASS (SCSS) to CSS
    autoprefixer = require('gulp-autoprefixer'), // Plugin for auto install autoprefixer
    cleanCSS = require('gulp-clean-css'), // Plugin for CSS minification
    uglify = require('gulp-uglify'), // Plugin for JavaScript minifucation
    cache = require('gulp-cache'), // Plugin for cache
    imagemin = require('gulp-imagemin'), // Plugin for compress PNG, JPEG, GIF, SVG pictures
    jpegrecompress = require('imagemin-jpeg-recompress'), // Plugin for compress JPEG
    pngquant = require('imagemin-pngquant'), // Plugin for compress PNG
    htmlmin = require('gulp-htmlmin'), // Plugin for compress HTML
    del = require('del'); // Plugin for delete files and directories

/* Params for gulp-autoprefixer */
var autoprefixerList = [
    'Chrome >= 45',
    'Firefox ESR',
    'Edge >= 12',
    'Explorer >= 10',
    'iOS >= 9',
    'Safari >= 9',
    'Android >= 4.4',
    'Opera >= 30'
];

/* Paths to source and build files */
var path = {
    build: {
        html:  './web/',
        js:    './web/build/js/',
        css:   './web/build/css/',
        images: {
            base:     './web/build/img/',
            jqueryui: './web/build/img/',
            fancybox: './web/build/img/',
            slick:    './web/build/css/'
        },
        fonts: {
            base:  './web/build/fonts/',
            slick: './web/build/css/fonts/'
        }
    },
    src: {
        html: './resources/html/*.html',
        js: {
            vendor: './resources/assets/js/vendor/vendor.js',
            app:    './resources/assets/js/app/app.js'
        },
        css: {
            vendor: './resources/assets/style/vendor/vendor.scss',
            app:    './resources/assets/style/app/app.scss'
        },
        images: {
            base:     './resources/assets/img/**/*.*',
            jqueryui: './node_modules/jquery-ui/themes/base/images/*.png',
            fancybox: './node_modules/fancybox/dist/img/*.*',
            slick:    './node_modules/slick-carousel/slick/ajax-loader.gif'
        },
        fonts: {
            base: [
                './resources/assets/fonts/**/*.*',
                './node_modules/font-awesome/fonts/*.*',
                './node_modules/bootstrap-sass/assets/fonts/bootstrap/*.*'
            ],
            slick: './node_modules/slick-carousel/slick/fonts/*.*'
        }
    },
    watch: {
        html: './resources/html/**/*.html',
        js: {
            vendor: './resources/assets/js/vendor/**/*.js',
            app:    './resources/assets/js/app/**/*.js'
        },
        css: {
            vendor: './resources/assets/style/vendor/**/*.scss',
            app:    './resources/assets/style/app/**/*.scss'
        },
        img:   './resources/assets/img/**/*.*',
        fonts: './resources/assets/fonts/**/*.*'
    },
    clean: ['./web/build','./web/*.html']
};

/* Config Server */
var config = {
    server: {
        baseDir: './web'
    },
    notify: false
};

/* Tasks */

// Run server
gulp.task('webserver', function () {
    webserver(config);
});

// Build HTML files
gulp.task('html', function() {
    gulp.src(path.src.html)
        .pipe(fileinclude()) // build html files
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(path.build.html)) // выгружаем в web
        .pipe(webserver.reload({stream: true})); // перезагрузим сервер
});

// Build Styles
gulp.task('css', [
    'css:vendor',
    'css:app'
]);

// Build JavaScripts
gulp.task('js', [
    'js:vendor',
    'js:app'
]);

// Minify vendors styles
gulp.task('css:vendor', function () {
    gulp.src(path.src.css.vendor) // получим vendor.css
        .pipe(plumber()) // для отслеживания ошибок
        .pipe(sourcemaps.init()) // инициализируем sourcemap
        .pipe(sass()) // импортируем все указанные файлы в vendor.css
        .pipe(autoprefixer({ // добавим префиксы
            browsers: autoprefixerList
        }))
        .pipe(cleanCSS()) // минимизируем CSS
        .pipe(sourcemaps.write('./')) // записываем sourcemap
        .pipe(gulp.dest(path.build.css)) // выгружаем в build
        .pipe(webserver.reload({stream: true})); // перезагрузим сервер
});

// Minify vendors javascripts
gulp.task('js:vendor', function () {
    gulp.src(path.src.js.vendor) // получим файл vendor.js
        .pipe(plumber()) // для отслеживания ошибок
        .pipe(include()) // импортируем все указанные файлы в vendor.js
        .pipe(sourcemaps.init()) //инициализируем sourcemap
        .pipe(uglify()) // минимизируем js
        .pipe(sourcemaps.write('./')) //  записываем sourcemap
        .pipe(gulp.dest(path.build.js)) // положим готовый файл
        .pipe(webserver.reload({stream: true})); // перезагрузим сервер
});

// Minify app styles
gulp.task('css:app', function () {
    gulp.src(path.src.css.app) // получим app.css
        .pipe(plumber()) // для отслеживания ошибок
        .pipe(sourcemaps.init()) // инициализируем sourcemap
        .pipe(sass()) // импортируем все указанные файлы в app.css
        .pipe(autoprefixer({ // добавим префиксы
            browsers: autoprefixerList
        }))
        .pipe(cleanCSS()) // минимизируем CSS
        .pipe(sourcemaps.write('./')) // записываем sourcemap
        .pipe(gulp.dest(path.build.css)) // выгружаем в build
        .pipe(webserver.reload({stream: true})); // перезагрузим сервер
});

// Minify app javascripts
gulp.task('js:app', function () {
    gulp.src(path.src.js.app) // получим файл app.js
        .pipe(plumber()) // для отслеживания ошибок
        .pipe(include()) // импортируем все указанные файлы в app.js
        .pipe(sourcemaps.init()) //инициализируем sourcemap
        .pipe(uglify()) // минимизируем js
        .pipe(sourcemaps.write('./')) //  записываем sourcemap
        .pipe(gulp.dest(path.build.js)) // положим готовый файл
        .pipe(webserver.reload({stream: true})); // перезагрузим сервер
});

// Copy user fonts
gulp.task('fonts:base', function() {
    gulp.src(path.src.fonts.base)
        .pipe(gulp.dest(path.build.fonts.base));
});

// Copy required fonts for Slick
gulp.task('fonts:slick', function () {
    gulp.src(path.src.fonts.slick)
        .pipe(gulp.dest(path.build.fonts.slick));
});

// Copy and compress images
gulp.task('images:base', function () {
    gulp.src(path.src.images.base) // путь с исходниками картинок
        .pipe(cache(imagemin([ // сжатие изображений
            imagemin.gifsicle({interlaced: true}),
            jpegrecompress({
                progressive: true,
                max: 90,
                min: 80
            }),
            pngquant(),
            imagemin.svgo({plugins: [{removeViewBox: false}]})
        ])))
        .pipe(gulp.dest(path.build.images.base)); // выгрузка готовых файлов
});

// Copy required images for jQuery-UI
gulp.task('images:ui', function () {
    gulp.src(path.src.images.jqueryui).pipe(gulp.dest(path.build.images.jqueryui));
});

// Copy required images for Fancybox
gulp.task('images:fancybox', function () {
    gulp.src(path.src.images.fancybox).pipe(gulp.dest(path.build.images.fancybox));
});

// Copy required images for Slick
gulp.task('images:slick', function () {
    gulp.src(path.src.images.slick).pipe(gulp.dest(path.build.images.slick));
});

// Remove build catalog
gulp.task('clean', function () {
    del.sync(path.clean);
});

// Cache clean
gulp.task('cache:clear', function () {
    cache.clearAll();
});

// Build fonts
gulp.task('fonts', [
    'fonts:base',
    'fonts:slick'
]);

// Build pictures
gulp.task('images', [
    'images:base',
    'images:ui',
    'images:fancybox',
    'images:slick'
]);

// Build project
gulp.task('build', [
    'clean',
    'css',
    'js',
    'fonts',
    'images',
    'html'
]);

// Run tasks then files are change
gulp.task('watch', function() {
    gulp.watch(path.watch.html,       ['html']);
    gulp.watch(path.watch.css.vendor, ['css:vendor']);
    gulp.watch(path.watch.js.vendor,  ['js:vendor']);
    gulp.watch(path.watch.css.app,    ['css:app']);
    gulp.watch(path.watch.js.app,     ['js:app']);
    gulp.watch(path.watch.img,        ['images:base']);
    gulp.watch(path.watch.fonts,      ['fonts:base']);
});

// Default task
gulp.task('default', [
    'build',
    'webserver',
    'watch'
]);